<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Artist\ArtistCreateRequest;
use App\Http\Requests\Api\Artist\ArtistDeleteRequest;
use App\Http\Requests\Api\Artist\ArtistUpdateRequest;
use App\Models\Artist;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    public function getById($id){
        $artist = Artist::where('id', $id)->first();
        if(is_null($artist)){
            return response()->json([
                'errors' => 'Artist not found.'
            ], 403);
        }
        return response()->json([
            'artist' => $artist
        ]);
    }
    public function searchArtist(Request $request){
        $artists = Artist::where('name', 'like', "%".strtolower($request->get('keyword'))."%")->get();
        return response()->json([
            'artists' => $artists
        ]);
    }

    public function getAll(){
        return response()->json([
            'artists' => Artist::all()
        ]);
    }
    public function create(ArtistCreateRequest $request){
        Artist::create($request->all());
        return response()->json([
            'message' => 'Artist created'
        ]);
    }
    public function update(ArtistUpdateRequest $request){
        Artist::where('id', $request->get('id'))
            ->update($request->all());
        return response()->json([
            'message' => 'Artist updated'
        ]);
    }
    public function delete(ArtistDeleteRequest $request){
        Artist::where('id', $request->get('id'))
            ->delete();
        return response()->json([
            'message' => 'Artist deleted'
        ]);
    }
}
