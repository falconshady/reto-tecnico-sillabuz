<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\UserLoginRequest;
use App\Http\Requests\Api\User\UserLogoutRequest;
use App\Http\Requests\Api\User\UserRegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\PersonalAccessToken;

class UserController extends Controller
{
    public function login(UserLoginRequest $request){
        $user = User::where('email', $request->email)->first();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            return response()->json([
                'errors' => 'User not found.'
            ], 403);
        }

        return response()->json([
            'token' => $user->createToken($request->email)->plainTextToken
        ]);
    }

    public function register(UserRegisterRequest $request){
        $user = User::create($request->all());
        $token = $user->createToken($request->email)->plainTextToken;

        return response()->json([
            'user' => $user,
            'token' => $token
        ]);
    }

    public function logout(UserLogoutRequest $request){
        PersonalAccessToken::where('name', $request->get('email'))->delete();
        return response()->json([
            'message' => 'Successful logout'
        ]);
    }
}
