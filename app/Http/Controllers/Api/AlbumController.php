<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Album\AlbumCreateRequest;
use App\Http\Requests\Api\Album\AlbumDeleteRequest;
use App\Http\Requests\Api\Album\AlbumUpdateRequest;
use App\Models\Album;

class AlbumController extends Controller
{
    public function getById($id){
        $artist = Album::where('id', $id)->first();
        if(is_null($artist)){
            return response()->json([
                'errors' => 'Album not found.'
            ], 403);
        }
        return response()->json([
            'album' => $artist
        ]);
    }

    public function getAll(){
        return response()->json([
            'albums' => Album::all()
        ]);
    }
    public function create(AlbumCreateRequest $request){
        Album::create($request->all());
        return response()->json([
            'message' => 'Album created'
        ]);
    }
    public function update(AlbumUpdateRequest $request){
        Album::where('id', $request->get('id'))
            ->update($request->all());
        return response()->json([
            'message' => 'Album updated'
        ]);
    }
    public function delete(AlbumDeleteRequest $request){
        Album::where('id', $request->get('id'))
            ->delete();
        return response()->json([
            'message' => 'Album deleted'
        ]);
    }
}
