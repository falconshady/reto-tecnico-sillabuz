<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::post('/login', [\App\Http\Controllers\Api\UserController::class, 'login']);
Route::post('/register', [\App\Http\Controllers\Api\UserController::class, 'register']);
Route::post('/logout', [\App\Http\Controllers\Api\UserController::class, 'logout']);


Route::middleware('auth:sanctum')->group(function() {
    
    Route::get('/artists', [\App\Http\Controllers\Api\ArtistController::class, 'getAll']);
    Route::get('/artist/{id}', [\App\Http\Controllers\Api\ArtistController::class, 'getById']);
    Route::post('/artist/create', [\App\Http\Controllers\Api\ArtistController::class, 'create']);
    Route::post('/artist/update', [\App\Http\Controllers\Api\ArtistController::class, 'update']);
    Route::post('/artist/delete', [\App\Http\Controllers\Api\ArtistController::class, 'delete']);
    
    Route::get('/search', [\App\Http\Controllers\Api\ArtistController::class, 'searchArtist']);
    
    Route::get('/albums', [\App\Http\Controllers\Api\AlbumController::class, 'getAll']);
    Route::get('/album/{id}', [\App\Http\Controllers\Api\AlbumController::class, 'getById']);
    Route::get('/album/{id}', [\App\Http\Controllers\Api\AlbumController::class, 'getById']);
    Route::post('/album/create', [\App\Http\Controllers\Api\AlbumController::class, 'create']);
    Route::post('/album/update', [\App\Http\Controllers\Api\AlbumController::class, 'update']);
    Route::post('/album/delete', [\App\Http\Controllers\Api\AlbumController::class, 'delete']);
});
    
