<?php

namespace Tests\Feature;
use Tests\TestCase;

class UserLogoutTest extends TestCase
{
    private $route = '/api/logout';
    private $params = [
        'email' => 'test@example.com',
    ];
    private $validationJson = ['email'];
    private $responseJson = ['message'];
    /**
     * A basic feature test example.
     */
    public function test_exist(): void
    {
        $this->post($this->route)->assertUnprocessable();
    }

    public function test_validations(): void
    {
        $this->post($this->route)->assertJsonValidationErrors($this->validationJson);
    }

    public function test_with_params(): void
    {
        $this->post($this->route, $this->params)->assertSuccessful()
        ->assertJsonStructure($this->responseJson);
    }
}
