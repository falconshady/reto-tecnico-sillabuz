<?php

namespace Tests\Feature;
use Tests\TestCase;

class UserRegisterTest extends TestCase
{
    private $route = '/api/register';
    private $params = [
        'name' => 'Freddy Silabuz',
        'email' => 'hola@silabuz.com',
        'password' => 'password',
        'confirm' => 'password',
        'country' => 'Perú',
    ];
    private $validationJson = ['name', 'email', 'password', 'confirm', 'country'];
    private $responseJson = ['token'];
    /**
     * A basic feature test example.
     */
    public function test_exist(): void
    {
        $this->post($this->route)->assertUnprocessable();
    }

    public function test_validations(): void
    {
        $this->post($this->route)->assertJsonValidationErrors($this->validationJson);
    }

    public function test__not_same_password(): void
    {
        $this->params['password'] = '123456789';
        $position = array_search('confirm', $this->validationJson);
        $this->post($this->route, $this->params)->assertUnprocessable()
            ->assertJsonValidationErrors($this->validationJson[$position]);
    }

    public function test_with_params(): void
    {
        $this->post($this->route, $this->params)->assertSuccessful()
        ->assertJsonStructure($this->responseJson);
    }

    public function test_already_exist_email(): void
    {
        $position = array_search('email', $this->validationJson);
        $this->post($this->route, $this->params)->assertUnprocessable()
            ->assertJsonValidationErrors($this->validationJson[$position]);
    }    
}
