<?php

namespace Tests\Feature;
use Tests\TestCase;

class ArtistByIdTest extends TestCase
{
    private $route = '/api/artist/1';
    private $routeLogin = '/api/login';
    private $params = [
        'email' => 'test@example.com',
        'password' => 'password',
    ];
    private $responseJson = ['artist'];
    /**
     * A basic feature test example.
     */
    public function test_exist(): void
    {
        $response = $this->get($this->route);
        $response->assertFound();
    }

    public function test_failed_token(): void
    {
        $this->withToken('')->get($this->route)
            ->assertFound();
    }

    public function test_get_list(): void
    {
        $login = $this->post($this->routeLogin, $this->params);
        $this->withToken($login->json('token'))->get($this->route)
            ->assertJsonStructure($this->responseJson);
    }
}
