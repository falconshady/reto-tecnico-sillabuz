<?php

namespace Tests\Feature;
use Tests\TestCase;

class UserLoginTest extends TestCase
{
    private $route = '/api/login';
    private $params = [
        'email' => 'test@example.com',
        'password' => 'password',
    ];
    private $validationJson = ['email', 'password'];
    private $responseJson = ['token'];
    /**
     * A basic feature test example.
     */
    public function test_exist(): void
    {
        $this->post($this->route)->assertUnprocessable();
    }

    public function test_validations(): void
    {
        $this->post($this->route)->assertJsonValidationErrors($this->validationJson);
    }

    public function test_with_params(): void
    {
        $this->post($this->route, $this->params)->assertSuccessful()
        ->assertJsonStructure($this->responseJson);
    }
}
