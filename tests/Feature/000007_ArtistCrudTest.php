<?php

use Tests\TestCase;

class AlbumCrudTest extends TestCase
{
    private $routeCreate = '/api/album/create';
    private $routeUpdate = '/api/album/update';
    private $routeDelete = '/api/album/delete';
    private $routeLogin = '/api/login';
    private $paramsLogin = [
        'email' => 'test@example.com',
        'password' => 'password',
    ];
    private $paramsCreate = [
        'name' => 'postumus ebla',
        'picture' => 'https://via.placeholder.com/640x480.png/00bb99',
        'artist_id' => 10,
    ];
    private $paramsUpdate = [
        'id' => 10,
        'name' => 'postumus pompeii',
        'picture' => 'https://via.placeholder.com/640x480.png/00bb99',
        'artist_id' => 10,
    ];
    private $paramsDelete = [
        'id' => 10,
    ];
    private $responseJson = ['message'];
    /**
     * A basic feature test example.
     */
    public function test_exist_create_route(): void
    {
        $response = $this->get($this->routeCreate);
        $response->assertFound();
    }

    public function test_exist_update_route(): void
    {
        $response = $this->get($this->routeUpdate);
        $response->assertFound();
    }

    public function test_exist_delete_route(): void
    {
        $response = $this->get($this->routeDelete);
        $response->assertFound();
    }

    public function test_failed_token_create_route(): void
    {
        $this->withToken('')->get($this->routeCreate)
            ->assertFound();
    }

    public function test_failed_token_update_route(): void
    {
        $this->withToken('')->get($this->routeUpdate)
            ->assertFound();
    }

    public function test_failed_token_delete_route(): void
    {
        $this->withToken('')->get($this->routeDelete)
            ->assertFound();
    }

    public function test_create_route(): void
    {
        $login = $this->post($this->routeLogin, $this->paramsLogin);
        $this->withToken($login->json('token'))->post($this->routeCreate, $this->paramsCreate)
            ->assertJsonStructure($this->responseJson);
    }

    public function test_update_route(): void
    {
        $login = $this->post($this->routeLogin, $this->paramsLogin);
        $this->withToken($login->json('token'))->post($this->routeUpdate, $this->paramsUpdate)
            ->assertJsonStructure($this->responseJson);
    }

    public function test_delete_route(): void
    {
        $login = $this->post($this->routeLogin, $this->paramsLogin);
        $this->withToken($login->json('token'))->post($this->routeDelete, $this->paramsDelete)
            ->assertJsonStructure($this->responseJson);
    }
}
